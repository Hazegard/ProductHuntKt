package fr.ec.producthuntkt.model.wrapper

import fr.ec.producthuntkt.model.retrofit.Topic

/**
 * Created by maxime on 15/02/18.
 */

data class ListTopic(var topics: List<Topic>)