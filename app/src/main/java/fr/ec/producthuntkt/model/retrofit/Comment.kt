package fr.ec.producthuntkt.model.retrofit

import java.util.*

/**
 * Created by maxime on 17/02/18.
 */

data class Comment(
        var id: Int,
        var subject_type: String,
        var sticky: Boolean,
        var user: User,
        var votes: Int,
        var post: Post,
        var child_comments: List<Comment>,
        var maker: Boolean,
        var hunter: Boolean,
        var live_guest: Boolean,
        var body: String,
        var created_at: Date,
        var parent_comment_id: Int,
        var user_id: Int,
        var subject_id: Int,
        var url: String,
        var post_id: Int
)