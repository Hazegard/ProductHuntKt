package fr.ec.producthuntkt.model.wrapper

import fr.ec.producthuntkt.model.retrofit.Post

/**
 * Created by maxime on 17/02/18.
 */

data class WrapperListPosts(var posts: List<Post>)