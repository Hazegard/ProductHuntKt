package fr.ec.producthuntkt.model.retrofit

import java.util.*

/**
 * Created by maxime on 17/02/18.
 */
data class Post(
        var id: Int,
        var tagline: String,
        var slug: String,
        var category_id: Int,
        var created_at: Date,
        var discussion_url: String,
        var makers: List<User>,
        var user: User,
        var thumbnail: Thumbnail,
        var topics: List<Topic>,
        var comments_count: Int,
        var day: Date,
        var name: String,
        var product_state: String,
        var votes_count: Int,
        val comments: List<Comment>,
        val related_posts: List<Post>,
        var description: String,
        val media: List<Media>
)