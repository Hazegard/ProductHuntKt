package fr.ec.producthuntkt.model.retrofit

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*

/**
 * Created by hazegard on 13/03/18.
 */
@Entity(tableName = "POST")
data class MiniPost(
        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: Int,
        @ColumnInfo(name = "tagline")
        var tagline: String,
        @ColumnInfo(name = "image_url")
        var image_url: String,
        @ColumnInfo(name = "comments_count")
        var comments_count: Int,
        @ColumnInfo(name = "name")
        var name: String,
        @ColumnInfo(name = "votes_count")
        var votes_count: Int,
        @ColumnInfo(name = "created_at")
        var created_at: Date,
        @ColumnInfo(name = "topic_id")
        val topic_id: Int,
        @ColumnInfo(name = "collection_id")
        val collection_id: Int
) {
    constructor(post: Post, topic_id: Int, collection_id: Int) : this(
            post.id,
            post.tagline,
            post.thumbnail.image_url,
            post.comments_count,
            post.name,
            post.votes_count,
            post.created_at,
            topic_id,
            collection_id
    )

    enum class PostsType(val value: Int) {
        ALL(0),
        TOPICS(1),
        COLLECTION(2)

    }
}