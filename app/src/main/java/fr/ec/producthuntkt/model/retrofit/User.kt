package fr.ec.producthuntkt.model.retrofit

import java.util.*

/**
 * Created by maxime on 17/02/18.
 */
data class User(
        var id: Int,
        var image_url: Map<String, String>,
        var created_at: Date,
        var name: String,
        var username: String,
        var headline: String,
        var website_url: String,
        var twitter_username: String,
        var profile_url: String
)