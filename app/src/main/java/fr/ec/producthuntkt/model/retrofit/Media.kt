package fr.ec.producthuntkt.model.retrofit

/**
 * Created by maxime on 17/02/18.
 */
data class Media(
        var id: Int,
        var media_types: String,
        var original_width: Int,
        var original_height: Int,
        var image_url: String
)