package fr.ec.producthuntkt.model.wrapper

import fr.ec.producthuntkt.model.retrofit.Post

/**
 * Created by maxime on 20/02/18.
 */
data class WrapperPostDetail(var post: Post)