package fr.ec.producthuntkt.model.retrofit

/**
 * Created by maxime on 17/02/18.
 */

data class Thumbnail(
        var id: Int,
        var image_url: String,
        var media_type: String
)