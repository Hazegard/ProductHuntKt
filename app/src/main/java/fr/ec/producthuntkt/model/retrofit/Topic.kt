package fr.ec.producthuntkt.model.retrofit

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.*

/**
 * Created by maxime on 15/02/18.
 */
@Entity(tableName = "TOPIC")
data class Topic(
        @ColumnInfo(name = "id")
        @PrimaryKey
        var id: Int,
        @ColumnInfo(name = "name")
        var name: String,
        @ColumnInfo(name = "slug")
        var slug: String,
        @ColumnInfo(name = "created_at")
        var created_at: Date,
        @ColumnInfo(name = "description")
        var description: String,
        @ColumnInfo(name = "followers_count")
        var followers_count: Int,
        @ColumnInfo(name = "posts_count")
        var posts_count: Int,
        @ColumnInfo(name = "trending")
        var trending: Boolean,
        @ColumnInfo(name = "updated_at")
        var updated_at: Date,
        @ColumnInfo(name = "image")
        var image: String
) {
    var isDescriptionEllipsed: Boolean = false
}