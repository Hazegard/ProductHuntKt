package fr.ec.producthuntkt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.ec.producthuntkt.adapter.TopicsListAdapter
import fr.ec.producthuntkt.interfaces.OnItemClicked
import fr.ec.producthuntkt.model.retrofit.MiniPost
import fr.ec.producthuntkt.model.retrofit.Topic
import fr.ec.producthuntkt.repository.Controller
import fr.ec.producthuntkt.repository.SyncService
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.topics_fragment.*

/**
 * Created by hazegard on 12/03/18.
 */
class TopicFragment : Fragment() {
    private val controller by lazy { Controller.getInstance(context!!) }
    private val syncTopicReceiver: SyncTopicReceiver = SyncTopicReceiver()
    private lateinit var topicListAdapter: TopicsListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.topics_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refresh_topics.isRefreshing = true
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(
                context, LinearLayoutManager.VERTICAL, false)
        topics_view.layoutManager = layoutManager
        refresh_topics.setOnRefreshListener {
            SyncService.startSyncTopics(context!!)
        }
        topicListAdapter = TopicsListAdapter(ArrayList())
        topics_view.adapter = topicListAdapter
        topicListAdapter.onClick = object : OnItemClicked<Topic> {
            override fun onClick(item: Topic) {
                val intent = Intent(context, ListPostActivity::class.java)
                intent.putExtra(ListPostActivity.TOPIC_ID, item.id)
                intent.putExtra(ListPostActivity.TOPIC_NAME, item.name)
                intent.putExtra(ListPostActivity.TYPE_POSTS, MiniPost.PostsType.TOPICS)
                startActivity(intent)
            }
        }
        SyncService.startSyncTopics(context!!)
        loadTopics(false)
    }

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter()
        intentFilter.addAction(ACTION_LOAD_TOPICS)
        LocalBroadcastManager.getInstance(this.context!!).registerReceiver(syncTopicReceiver, intentFilter)
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this.context!!).unregisterReceiver(syncTopicReceiver)
    }

    fun loadTopics(afterSync: Boolean) {
        controller.getTopics()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    topicListAdapter.updateList(it.sortedByDescending { it.created_at })
                }
        if (afterSync) {
            refresh_topics.isRefreshing = false
        }
    }

    inner class SyncTopicReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent?) {
            if (intent?.action == ACTION_LOAD_TOPICS) {
                loadTopics(true)
            }
        }

    }

    companion object {
        const val ACTION_LOAD_TOPICS = "fr.ec.producthunt.data.action.LOAD_TOPICS"
        fun getNewInstance(): TopicFragment {
            return TopicFragment()
        }
    }
}
