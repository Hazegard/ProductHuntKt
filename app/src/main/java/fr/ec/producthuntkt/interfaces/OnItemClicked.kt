package fr.ec.producthuntkt.interfaces

/**
 * Created by maxime on 16/02/18.
 */
interface OnItemClicked<in T> {
    fun onClick(item: T)
}