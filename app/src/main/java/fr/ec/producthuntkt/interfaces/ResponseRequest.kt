package fr.ec.producthuntkt.interfaces

/**
 * Created by maxime on 15/02/18.
 */
interface ResponseRequest<in T> {
    fun onSuccess(response: T)
    fun onFailure(message: String?)
}