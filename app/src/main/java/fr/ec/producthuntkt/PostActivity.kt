package fr.ec.producthuntkt

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.post_view.*
import kotlinx.android.synthetic.main.toolbarlayout.*
import fr.ec.producthuntkt.adapter.CommentsAdapter
import fr.ec.producthuntkt.interfaces.ResponseRequest
import fr.ec.producthuntkt.model.retrofit.Post
import fr.ec.producthuntkt.repository.network.ApiController

/**
 * Created by maxime on 20/02/18.
 */
class PostActivity : AppCompatActivity() {
    private val apiController: ApiController = ApiController()
    lateinit var commentsAdapter: CommentsAdapter
    private var postId: Int = 0
    private var postName: String = ""
    var post: Post? = null
    private val responseCommentsCb = object : ResponseRequest<Post> {
        override fun onSuccess(response: Post) {
            commentsAdapter = CommentsAdapter(response)
            postView_comments.adapter = commentsAdapter
            Log.d(TAG, response.toString())
            commentsAdapter.updateContent(response)
            post = response
            post_refresh_layout.isRefreshing = false
        }

        override fun onFailure(message: String?) {
            post_refresh_layout.isRefreshing = false
            Log.d(TAG, message)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.post_view)
        post_refresh_layout.isRefreshing = true
        postId = intent?.extras?.getInt(POST_ID) ?: 0
        postName = intent?.extras?.getString(POST_NAME) ?: ""
        toolbar.title = postName
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        if (postId != 0) {
            apiController.getPostDetail(postId, responseCommentsCb)
        }
        post_refresh_layout.setOnRefreshListener {
            if (postId != 0) {
                apiController.getPostDetail(postId, responseCommentsCb)
            }
        }
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false)
        postView_comments.layoutManager = layoutManager
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.post_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.open_in_browser -> {
                val url = post?.discussion_url
                if (url != null) {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(url)
                    startActivity(intent)
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val POST_ID = "POST_ID"
        const val POST_NAME = "POST_NAME"
        private const val TAG: String = "PostActivity"
    }
}
