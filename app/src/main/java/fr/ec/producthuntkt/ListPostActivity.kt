package fr.ec.producthuntkt

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import fr.ec.producthuntkt.model.retrofit.MiniPost
import kotlinx.android.synthetic.main.toolbarlayout.*

/**
 * Created by maxime on 17/02/18.
 */
class ListPostActivity : AppCompatActivity() {
    private var topicId: Int = 0
    private var topicName: String = ""
    private lateinit var type: MiniPost.PostsType

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.posts_list_activity)

        topicId = intent.extras?.getInt(TOPIC_ID) ?: 0
        type = (intent.extras?.getSerializable(TYPE_POSTS) as MiniPost.PostsType?) ?: MiniPost.PostsType.ALL
        Log.d(TAG, topicId.toString())
        supportFragmentManager.beginTransaction()
                .add(R.id.postLists_container, ListPostFragment.getNewInstance(topicId, type))
                .commit()

        topicName = intent.extras?.getString(TOPIC_NAME) ?: ""
        toolbar.title = topicName
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    companion object {
        const val TYPE_POSTS = "TYPE_POSTS"
        const val TOPIC_NAME = "TOPIC_NAME"
        const val TOPIC_ID = "TOPIC_ID"
        const val TAG: String = "ListPostActivity"
    }
}
