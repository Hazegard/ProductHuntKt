package fr.ec.producthuntkt

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import fr.ec.producthuntkt.adapter.PostsListAdapter
import fr.ec.producthuntkt.interfaces.OnItemClicked
import fr.ec.producthuntkt.model.retrofit.MiniPost
import fr.ec.producthuntkt.repository.Controller
import fr.ec.producthuntkt.repository.SyncNewService
import fr.ec.producthuntkt.repository.SyncService
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.posts_list.*
import java.util.*

/**
 * Created by hazegard on 12/03/18.
 */
class ListPostFragment : Fragment() {
    private val synPostsReceiver: SyncPostReceiver = SyncPostReceiver()
    lateinit var postsListAdapter: PostsListAdapter
    private var groupPostId = 0
    private var TYPE: MiniPost.PostsType = MiniPost.PostsType.ALL
    private val controller: Controller by lazy { Controller.getInstance(context!!) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.posts_list, container, false)
        groupPostId = arguments?.getInt(ID) ?: 0
        TYPE = arguments?.getSerializable(TYPE_POSTS) as MiniPost.PostsType
        Log.d(TAG, groupPostId.toString())
        SyncService.syncPosts(context!!, TYPE, groupPostId)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        posts_swipe.setOnRefreshListener {
            SyncNewService.syncNewPosts(context!!, TYPE, postsListAdapter.getLastPostId(), groupPostId)
        }
        posts_swipe.isRefreshing = true
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(
                context, LinearLayoutManager.VERTICAL, false)
        posts_list.layoutManager = layoutManager
        postsListAdapter = PostsListAdapter(Collections.emptyList())
        posts_list.adapter = postsListAdapter
        postsListAdapter.onClick = onPostClick
        SyncService.syncPosts(context!!, TYPE, groupPostId)
        loadPosts(false)
    }

    private val onPostClick: OnItemClicked<MiniPost> = object : OnItemClicked<MiniPost> {
        override fun onClick(item: MiniPost) {
            val intent = Intent(context, PostActivity::class.java)
            intent.putExtra(PostActivity.POST_ID, item.id)
            intent.putExtra(PostActivity.POST_NAME, item.name)
            startActivity(intent)
        }
    }

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter()
        intentFilter.addAction(ACTION_LOAD_POSTS)
        LocalBroadcastManager.getInstance(this.context!!).registerReceiver(synPostsReceiver, intentFilter)
    }

    override fun onStop() {
        super.onStop()
        LocalBroadcastManager.getInstance(this.context!!).unregisterReceiver(synPostsReceiver)
    }

    fun loadPosts(afterSync: Boolean) {
        controller.getPosts(groupPostId, TYPE).observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    postsListAdapter.updateContent(it.sortedByDescending { it.created_at })
                    if (afterSync) {
                        posts_swipe?.isRefreshing = false
                    }
                }
    }

    inner class SyncPostReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent?.action == ACTION_LOAD_POSTS) {
                loadPosts(true)
            }
        }
    }

    companion object {
        const val ACTION_LOAD_POSTS = "fr.ec.producthunt.data.action.LOAD_POSTS"
        const val TAG = "ListPostFragment"
        const val ID = "TOPIC_ID"
        const val TYPE_POSTS = "TYPE"
        fun getNewInstance(id: Int, type: MiniPost.PostsType): ListPostFragment {
            val fragment = ListPostFragment()
            val bundle = Bundle()
            bundle.putInt(ID, id)
            bundle.putSerializable(TYPE_POSTS, type)
            fragment.arguments = bundle
            return fragment
        }
    }
}