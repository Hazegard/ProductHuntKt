package fr.ec.producthuntkt.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import fr.ec.producthuntkt.R
import fr.ec.producthuntkt.interfaces.OnItemClicked
import fr.ec.producthuntkt.model.retrofit.MiniPost
import kotlinx.android.synthetic.main.row_post.view.*

/**
 * Created by maxime on 17/02/18.
 */
class PostsListAdapter(private var posts: List<MiniPost>) : RecyclerView.Adapter<PostsListAdapter.PostHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostHolder {
        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_post, parent, false)
        return PostHolder(itemView)
    }

    override fun getItemCount(): Int {
        return posts.size
    }

    fun getLastPostId(): Int {
        return posts[0].id
    }

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        val post: MiniPost = posts[position]
        holder.updateContent(post)
        holder.postCard.setOnClickListener { this.onClick.onClick(post) }
    }

    lateinit var onClick: OnItemClicked<MiniPost>
    fun updateContent(posts: List<MiniPost>) {
        this.posts = posts
        notifyDataSetChanged()
    }

    class PostHolder(private var view: View) : RecyclerView.ViewHolder(view) {
        val postCard: CardView = with(view) { post_view }

        fun updateContent(post: MiniPost) = with(view) {
            post_title.text = post.name
            post_tagline.text = post.tagline
            post_nb_upvote.text = String.format("🔼 %s", post.votes_count)
            Glide.with(context).load(post.image_url).into(post_image)
            post_nb_comments.text = String.format("\uD83D\uDCAC %s", post.comments_count)

        }

    }
}