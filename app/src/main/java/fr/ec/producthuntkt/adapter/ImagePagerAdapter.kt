package fr.ec.producthuntkt.adapter

import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import fr.ec.producthuntkt.R
import fr.ec.producthuntkt.model.retrofit.Media
import kotlinx.android.synthetic.main.row_image.view.*

/**
 * Created by maxime on 20/02/18.
 */
class ImagePagerAdapter(private var mediaList: List<Media>) : PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return mediaList.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView: View = LayoutInflater.from(container.context)
                .inflate(R.layout.row_image, container, false)

        Glide.with(container.context).load(mediaList[position].image_url)
                .into(with(itemView) { image_post })
        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }

    fun updateContent(mediaList: List<Media>) {
        this.mediaList = mediaList
        notifyDataSetChanged()
    }
}