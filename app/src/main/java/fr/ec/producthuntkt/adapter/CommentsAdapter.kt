package fr.ec.producthuntkt.adapter

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import fr.ec.producthuntkt.R
import fr.ec.producthuntkt.model.retrofit.Comment
import fr.ec.producthuntkt.model.retrofit.Post
import kotlinx.android.synthetic.main.post.view.*
import kotlinx.android.synthetic.main.row_comment.view.*

/**
 * Created by maxime on 20/02/18.
 */
class CommentsAdapter(private var comments: List<Comment>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is PostHolder) {
            holder.updateContent(post!!)
        } else if (holder is CommentHolder) {
            val comment: Comment = if (isSubComment) comments[position] else comments[position - 1]
            holder.updateContent(comment)
        }
    }

    private var isSubComment: Boolean = true
    var post: Post? = null

    constructor(post: Post) : this(post.comments) {
        isSubComment = false
        this.post = post
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == TYPE_HEADER && !isSubComment) {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.post, parent, false)
            PostHolder(view, post!!)
        } else {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.row_comment, parent, false)
            CommentHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (position == 0 && !isSubComment) {
            return Companion.TYPE_HEADER
        }
        return Companion.TYPE_OTHER
    }

    override fun getItemCount(): Int {
        if (isSubComment) {
            return comments.size
        }
        return comments.size + 1
    }

    fun updateContent(post: Post) {
        this.post = post
        this.comments = post.comments
        notifyDataSetChanged()
    }

    class CommentHolder(private var view: View) : RecyclerView.ViewHolder(view) {
        private lateinit var subCommentAdapter: CommentsAdapter
        fun updateContent(comment: Comment) = with(view) {
            comment_author_name.text = comment.user.name
            comment_content.text = comment.body
            comment_date.text = comment.created_at.toString()
            comment_upvote.text = "👍 ${comment.votes}"
            Glide.with(context).load(comment.user.image_url["96px"]).into(comment_author_picture)
            if (comment.child_comments.isNotEmpty()) {
                subCommentAdapter = CommentsAdapter(comment.child_comments)
                val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context,
                        LinearLayoutManager.VERTICAL, false)
                comment_subcomments.layoutManager = layoutManager
                comment_subcomments.visibility = View.VISIBLE
                comment_subcomments.adapter = subCommentAdapter
            }
        }
    }

    class PostHolder(private var view: View, var post: Post) : RecyclerView.ViewHolder(view) {
        private var imagePagerAdapter: ImagePagerAdapter? = null

        init {
            if (post.media != null) {
                imagePagerAdapter = ImagePagerAdapter(post.media!!)
            }
            with(view) {
                post_imagePager.adapter = imagePagerAdapter
            }
        }

        fun updateContent(post: Post) = with(view) {
            postView_description.text = post.description
            postView_tagline.text = post.tagline
            postView_title.text = post.name
            if (imagePagerAdapter != null && post.media != null) {
                imagePagerAdapter?.updateContent(post.media)
            }
        }
    }

    companion object {
        private const val TYPE_HEADER: Int = 0
        private const val TYPE_OTHER: Int = 1
    }
}