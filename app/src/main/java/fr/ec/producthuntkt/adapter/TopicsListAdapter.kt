package fr.ec.producthuntkt.adapter

import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import fr.ec.producthuntkt.R
import fr.ec.producthuntkt.interfaces.OnItemClicked
import fr.ec.producthuntkt.model.retrofit.Topic
import kotlinx.android.synthetic.main.row_topics.view.*

/**
 * Created by maxime on 15/02/18.
 */

class TopicsListAdapter(private var topics: List<Topic>) : RecyclerView.Adapter<TopicsListAdapter.TopicHolder>() {
    lateinit var onClick: OnItemClicked<Topic>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicHolder {
        val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_topics, parent, false)
        return TopicHolder(view)
    }

    override fun getItemCount(): Int {
        return topics.size
    }

    override fun onBindViewHolder(holder: TopicHolder, position: Int) {
        val topic: Topic = topics[position]
        holder.updateContent(topic)
        holder.topicCard.setOnClickListener { this.onClick.onClick(topic) }
        holder.topicCard.setOnLongClickListener {
            if (topic.isDescriptionEllipsed) {
                holder.topicDescription.maxLines = MAX_LINES
            } else {
                holder.topicDescription.maxLines = Int.MAX_VALUE
            }
            topic.isDescriptionEllipsed = !topic.isDescriptionEllipsed
            return@setOnLongClickListener true
        }
    }

    fun updateList(topics: List<Topic>) {
        this.topics = topics
        notifyDataSetChanged()
    }

    class TopicHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val topicCard: CardView = with(itemView) { topic_card }
        val topicDescription: TextView = with(itemView) { topic_description }
        fun updateContent(topic: Topic) = with(itemView) {
            topic_description.text = topic.description
            topic_name.text = topic.name
            topic_post_count.text = topic.posts_count.toString()
            topic_followers.text = topic.followers_count.toString()
            Glide.with(context).load(topic.image).into(topic_image)
        }
    }

    companion object {
        private const val MAX_LINES: Int = 3
    }
}