package fr.ec.producthuntkt.repository.network

import fr.ec.producthuntkt.interfaces.ResponseRequest
import fr.ec.producthuntkt.model.retrofit.MiniPost
import fr.ec.producthuntkt.model.retrofit.Post
import fr.ec.producthuntkt.model.wrapper.ListTopic
import fr.ec.producthuntkt.model.wrapper.WrapperListPosts
import fr.ec.producthuntkt.model.wrapper.WrapperPostDetail
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by maxime on 15/02/18.
 */

class ApiController {
    private var subscriber: Disposable? = null
    private val retrofit: Retrofit by lazy {
        Retrofit.Builder().baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    private val service by lazy {
        this.retrofit.create(ProductHuntService::class.java)
    }

    fun getTopics(): Flowable<ListTopic> {
        return service.listTopic()
    }


    fun getPostsOfTopic(topicId: Int): Flowable<List<MiniPost>> {
        return service.listPostsOfTopic(topicId).map {
            wrapperToMiniPost(it, topicId, 0)
        }
    }

    fun getNewerPostsOfTopic(lastPostID: Int, topicId: Int): Flowable<List<MiniPost>> {
        return service.getNewerPostsOfTopic(topicId, lastPostID).map {
            wrapperToMiniPost(it, topicId, 0)
        }
    }

    fun getNewerPosts(lastPostID: Int): Flowable<List<MiniPost>> {
        return service.getNewerPosts(lastPostID).map {
            wrapperToMiniPost(it, 0, 0)
        }
    }

    private val wrapperToMiniPost = { wrapperPost: WrapperListPosts, topicId: Int, collectionId: Int ->
        wrapperPost.posts.map {
            MiniPost(it, topicId, collectionId)
        }
    }

    fun getPostsOfCollection(collectionId: Int): Flowable<List<MiniPost>> {
        //TODO
        return service.listPostsOfTopic(collectionId).map {
            wrapperToMiniPost(it, 0, collectionId)
        }
    }

    fun getPostDetail(id: Int, callback: ResponseRequest<Post>) {
        val req: Flowable<WrapperPostDetail> = service.getPost(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
        subscriber = req.subscribe({ callback.onSuccess(it.post) },
                { callback.onFailure(it.message) })
    }

    fun getAllPosts(): Flowable<List<MiniPost>> {
        return service.getAllPosts().map {
            wrapperToMiniPost(it, 0, 0)
        }
    }


    companion object {
        private const val BASE_URL: String = "https://api.producthunt.com"
        private const val TAG = "ApiController"
    }

}
