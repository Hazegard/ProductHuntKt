package fr.ec.producthuntkt.repository.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import fr.ec.producthuntkt.model.retrofit.MiniPost
import io.reactivex.Flowable
import io.reactivex.Observable

/**
 * Created by hazegard on 13/03/18.
 */
@Dao
interface PostDao {
    @Query("SELECT * FROM POST")
    fun getAll(): Flowable<List<MiniPost>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(post: MiniPost)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg post: MiniPost)

    @Query("SELECT * FROM POST WHERE topic_id =:topicId")
    fun getPostsOfTopic(topicId: Int): Flowable<List<MiniPost>>

    @Query("SELECT * FROM POST WHERE collection_id=:collectionId")
    fun getPostsOfCollection(collectionId: Int): Flowable<List<MiniPost>>
}