//package producthuntkt.fr.ec.myapplication.repository.database.dao
//
//import android.arch.persistence.room.Dao
//import android.arch.persistence.room.Insert
//import android.arch.persistence.room.OnConflictStrategy
//import android.arch.persistence.room.Query
//import io.reactivex.Flowable
//import producthuntkt.fr.ec.myapplication.model.retrofit.Comment
//
///**
// * Created by maxime on 23/02/18.
// */
//@Dao
//interface CommentDao {
//    @Query("SELECT * FROM COMMENT")
//    fun getAll(): Flowable<List<Comment>>
//
//    @Query("SELECT * FROM COMMENT WHERE POST_ID=:postId")
//    fun getCommentByPostId(postId: Int): Flowable<List<Comment>>
//
//    @Query("SELECT * FROM COMMENT WHERE ID=:id")
//    fun getCommentById(id: Int): Flowable<List<Comment>>
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    fun insert(comment: Comment)
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    fun insertAll(vararg comment: Comment)
//}