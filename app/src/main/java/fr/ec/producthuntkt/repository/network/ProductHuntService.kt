package fr.ec.producthuntkt.repository.network

import fr.ec.producthuntkt.model.wrapper.ListTopic
import fr.ec.producthuntkt.model.wrapper.WrapperListPosts
import fr.ec.producthuntkt.model.wrapper.WrapperPostDetail
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by maxime on 15/02/18.
 */
interface ProductHuntService {
    @Headers("Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer f070e4c56ce98bc47d30f11b5f260fd2d6b7011d09c8cdb80468208321ee7d6f",
            "Host: api.producthunt.com")
    @GET("/v1/topics")
    fun listTopic(): Flowable<ListTopic>

    @Headers("Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer f070e4c56ce98bc47d30f11b5f260fd2d6b7011d09c8cdb80468208321ee7d6f",
            "Host: api.producthunt.com")
    @GET("/v1/posts/all")
    fun listPostsOfTopic(@Query("search[topic]") id: Int): Flowable<WrapperListPosts>

    @Headers("Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer f070e4c56ce98bc47d30f11b5f260fd2d6b7011d09c8cdb80468208321ee7d6f",
            "Host: api.producthunt.com")
    @GET("/v1/posts/all")
    fun getNewerPostsOfTopic(@Query("search[topic]") id: Int, @Query("newer") newerPostId: Int): Flowable<WrapperListPosts>

    @Headers("Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer f070e4c56ce98bc47d30f11b5f260fd2d6b7011d09c8cdb80468208321ee7d6f",
            "Host: api.producthunt.com")
    @GET("/v1/posts/all")
    fun getNewerPosts(@Query("newer") newerPostId: Int): Flowable<WrapperListPosts>

    @Headers("Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer f070e4c56ce98bc47d30f11b5f260fd2d6b7011d09c8cdb80468208321ee7d6f",
            "Host: api.producthunt.com")
    @GET("/v1/posts/{id}")
    fun getPost(@Path("id") postId: Int): Flowable<WrapperPostDetail>


    @Headers("Accept: application/json",
            "Content-Type: application/json",
            "Authorization: Bearer f070e4c56ce98bc47d30f11b5f260fd2d6b7011d09c8cdb80468208321ee7d6f")
    @GET("/v1/posts/all")
    fun getAllPosts(): Flowable<WrapperListPosts>
}
