package fr.ec.producthuntkt.repository.database.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import io.reactivex.Flowable
import fr.ec.producthuntkt.model.retrofit.Topic

/**
 * Created by maxime on 22/02/18.
 */
@Dao
interface TopicDao {
    @Query("SELECT * FROM TOPIC")
    fun getAll(): Flowable<List<Topic>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(topic: Topic)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(vararg topic: Topic)
}