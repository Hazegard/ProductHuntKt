package fr.ec.producthuntkt.repository

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import fr.ec.producthuntkt.ListPostFragment
import fr.ec.producthuntkt.ListPostFragment.Companion.TYPE_POSTS
import fr.ec.producthuntkt.TopicFragment
import fr.ec.producthuntkt.model.retrofit.MiniPost

/**
 * Created by hazegard on 14/03/18.
 */
class SyncService : IntentService("SyncService") {
    private val controller: Controller by lazy { Controller.getInstance(this) }
    override fun onHandleIntent(intent: Intent?) {
        val action: String = intent?.action ?: ""
        val id: Int = intent?.extras?.getInt(ID) ?: 0
        when (action) {
            ACTION_FETCH_NEW_POSTS_ALL -> {
                handleActionFetchPosts(0, MiniPost.PostsType.ALL)
            }
            ACTION_FETCH_NEW_TOPICS -> {
                handleActionFetchTopics()
            }
            ACTION_FETCH_NEW_POSTS_TOPICS -> {
                handleActionFetchPosts(id, MiniPost.PostsType.TOPICS)
            }
            ACTION_FETCH_NEW_POSTS_COLLECTION -> {
                handleActionFetchPosts(id, MiniPost.PostsType.COLLECTION)
            }
        }
    }

    private fun handleActionFetchTopics() {
        controller.saveTopics().subscribe({
            if (it != 0) {
                val intentToSend = Intent()
                intentToSend.action = TopicFragment.ACTION_LOAD_TOPICS
                LocalBroadcastManager.getInstance(this).sendBroadcastSync(intentToSend)
            }
        }, {})
    }

    private fun handleActionFetchPosts(id: Int, type: MiniPost.PostsType) {
        controller.saveAllPosts(id, type).subscribe {
            if (it != 0) {
                val intentToSend = Intent()
                intentToSend.action = ListPostFragment.ACTION_LOAD_POSTS
                intentToSend.putExtra(TYPE_POSTS, type)
                LocalBroadcastManager.getInstance(this).sendBroadcastSync(intentToSend)
            }
        }
    }

    companion object {
        private const val BASE_ACTION: String = "fr.ec.producthuntkt.repository"
        const val ACTION_FETCH_NEW_TOPICS: String = "$BASE_ACTION.FETCH_NEW_TOPICS"
        const val ACTION_FETCH_NEW_POSTS_TOPICS: String = "$BASE_ACTION.FETCH_NEW_POSTS_TOPICS"
        const val ACTION_FETCH_NEW_POSTS_COLLECTION: String = "$BASE_ACTION.FETCH_NEW_POSTS_COLLECTION"
        const val ACTION_FETCH_NEW_POSTS_ALL: String = "$BASE_ACTION.FETCH_NEW_POSTS"
        private const val ID = "ID"
        fun syncPosts(context: Context, type: MiniPost.PostsType, id: Int = 0) {
            val intent = Intent(context, SyncService::class.java)
            intent.action = when (type) {
                MiniPost.PostsType.ALL -> ACTION_FETCH_NEW_POSTS_ALL
                MiniPost.PostsType.TOPICS -> ACTION_FETCH_NEW_POSTS_TOPICS
                MiniPost.PostsType.COLLECTION -> ACTION_FETCH_NEW_POSTS_COLLECTION
            }
            intent.putExtra(ID, id)
            context.startService(intent)
        }

        fun startSyncTopics(context: Context) {
            val intent = Intent(context, SyncService::class.java)
            intent.action = ACTION_FETCH_NEW_TOPICS
            context.startService(intent)
        }
    }
}