//package producthuntkt.fr.ec.myapplication.repository.database.dao
//
//import android.arch.persistence.room.*
//import io.reactivex.Flowable
//import producthuntkt.fr.ec.myapplication.model.retrofit.User
//
///**
// * Created by maxime on 22/02/18.
// */
//@Dao
//interface UserDao {
//    @Query("SELECT * FROM USER")
//    fun getAll(): Flowable<List<User>>
//
//    @Query("SELECT * FROM USER WHERE ID=:userId")
//    fun getFromId(userId: Int): Flowable<List<User>>
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    fun insert(user: User)
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    fun insertAll(vararg user: User)
//
//    @Delete
//    fun delete(vararg user: User)
//}