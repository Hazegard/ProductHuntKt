package fr.ec.producthuntkt.repository

import android.arch.persistence.room.Room
import android.content.Context
import android.util.Log
import fr.ec.producthuntkt.model.retrofit.MiniPost
import fr.ec.producthuntkt.model.retrofit.Topic
import fr.ec.producthuntkt.repository.database.AppDatabase
import fr.ec.producthuntkt.repository.network.ApiController
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by maxime on 22/02/18.
 */
class Controller private constructor(context: Context) {
    private val dao by lazy {
        Room.databaseBuilder(context, AppDatabase::class.java,
                "database").build()
    }

    private val apiController: ApiController = ApiController()

    fun saveTopics(): Flowable<Int> {
        return Flowable.create({ emitter ->
            val topics = apiController.getTopics()
            topics.observeOn(Schedulers.io())
                    .subscribe({
                        dao.topicDao().insertAll(*it.topics.toTypedArray())
                    }, {})
            topics.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        emitter.onNext(it.topics.size)
                    }, {
                        emitter.onNext(-1)
                    })
        }, BackpressureStrategy.ERROR)
    }

    fun getTopics(): Flowable<List<Topic>> {
        return dao.topicDao().getAll()
    }

    fun saveNewPosts(currentLastPostId: Int, id: Int, type: MiniPost.PostsType): Flowable<Int> {
        return Flowable.create({ emitter ->
            val posts = if (id == 0) {
                apiController.getNewerPosts(currentLastPostId)
            } else {
                when (type) {
                    MiniPost.PostsType.TOPICS -> apiController.getNewerPostsOfTopic(currentLastPostId, id)
                    MiniPost.PostsType.COLLECTION -> apiController.getPostsOfCollection(id)
                    MiniPost.PostsType.ALL -> apiController.getNewerPosts(currentLastPostId)
                }
            }
            posts.observeOn(Schedulers.io())
                    .subscribe({
                        dao.miniPostDao().insertAll(*it.toTypedArray())
                        emitter.onNext(it.size)
                    }, {
                        Log.d("ControllerError", it.message)
                        emitter.onNext(-1)
                    })
        }, BackpressureStrategy.ERROR)
    }

    fun saveAllPosts(id: Int, type: MiniPost.PostsType): Flowable<Int> {
        return Flowable.create({ emitter ->
            val posts = if (id == 0) {
                apiController.getAllPosts()
            } else {
                when (type) {
                    MiniPost.PostsType.TOPICS -> apiController.getPostsOfTopic(id)
                    MiniPost.PostsType.COLLECTION -> apiController.getPostsOfCollection(id)
                    MiniPost.PostsType.ALL -> apiController.getAllPosts()
                }
            }
            posts.observeOn(Schedulers.io())
                    .subscribe({
                        dao.miniPostDao().insertAll(*it.toTypedArray())
                        emitter.onNext(it.size)
                    }, {
                        emitter.onNext(-1)
                        Log.d("ControllerError", it.message)
                    })
        }, BackpressureStrategy.ERROR)
    }

    fun getPosts(id: Int, type: MiniPost.PostsType): Flowable<List<MiniPost>> {
        return if (id == 0) {
            dao.miniPostDao().getAll()
        } else {
            when (type) {
                MiniPost.PostsType.ALL -> dao.miniPostDao().getAll()
                MiniPost.PostsType.TOPICS -> dao.miniPostDao().getPostsOfTopic(id)
                MiniPost.PostsType.COLLECTION -> dao.miniPostDao().getPostsOfCollection(id)
            }
        }
    }

    companion object {
        private var instance: Controller? = null
        fun getInstance(context: Context): Controller {
            if (instance == null) {
                instance = Controller(context)
            }
            return instance!!
        }
    }


}