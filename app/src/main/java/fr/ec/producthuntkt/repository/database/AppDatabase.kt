package fr.ec.producthuntkt.repository.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import fr.ec.producthuntkt.model.retrofit.MiniPost
import fr.ec.producthuntkt.model.retrofit.Topic
import fr.ec.producthuntkt.repository.database.dao.PostDao
import fr.ec.producthuntkt.repository.database.dao.TopicDao

/**
 * Created by maxime on 22/02/18.
 */
@TypeConverters(
        DateConverter::class
)
@Database(
        entities = [
            Topic::class,
            MiniPost::class
        ],
        version = 1,
        exportSchema = false)
abstract class AppDatabase : RoomDatabase() {
    abstract fun topicDao(): TopicDao
    abstract fun miniPostDao(): PostDao
}