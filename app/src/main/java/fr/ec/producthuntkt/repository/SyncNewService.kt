package fr.ec.producthuntkt.repository

import android.app.IntentService
import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import fr.ec.producthuntkt.ListPostFragment
import fr.ec.producthuntkt.ListPostFragment.Companion.TYPE_POSTS
import fr.ec.producthuntkt.model.retrofit.MiniPost

/**
 * Created by hazegard on 14/03/18.
 */
class SyncNewService : IntentService("SyncNewService") {
    private val controller: Controller by lazy { Controller.getInstance(this) }
    override fun onHandleIntent(intent: Intent?) {
        val action: String? = intent?.action
        val id: Int = intent?.extras?.getInt(ID) ?: 0
        val lastId: Int = intent?.extras?.getInt(LAST_ID) ?: 0
        when (action) {
            ACTION_FETCH_NEW_POSTS_ALL -> {
                if (lastId != 0)
                    handleActionFetchNewPosts(lastId, MiniPost.PostsType.ALL, 0)
            }
            ACTION_FETCH_NEW_POSTS_TOPICS -> {
                if (lastId != 0)
                    handleActionFetchNewPosts(lastId, MiniPost.PostsType.TOPICS, id)
            }
            ACTION_FETCH_NEW_POSTS_COLLECTION -> {
                if (lastId != 0)
                    handleActionFetchNewPosts(lastId, MiniPost.PostsType.COLLECTION, id)
            }
        }
    }

    private fun handleActionFetchNewPosts(lastId: Int, type: MiniPost.PostsType, id: Int) {
        controller.saveNewPosts(lastId, id, type).subscribe {
            val intentToSend = Intent()
            intentToSend.action = ListPostFragment.ACTION_LOAD_POSTS
            intentToSend.putExtra(TYPE_POSTS, type)
            LocalBroadcastManager.getInstance(this).sendBroadcastSync(intentToSend)
        }
    }

    companion object {
        private const val BASE_ACTION: String = "fr.ec.producthuntkt.repository"
        const val ACTION_FETCH_NEW_POSTS_TOPICS: String = "$BASE_ACTION.FETCH_POSTS_TOPICS"
        const val ACTION_FETCH_NEW_POSTS_COLLECTION: String = "$BASE_ACTION.FETCH_POSTS_COLLECTION"
        const val ACTION_FETCH_NEW_POSTS_ALL: String = "$BASE_ACTION.FETCH_POSTS"
        private const val ID = "ID"
        private const val LAST_ID = "LAST_ID"
        fun syncNewPosts(context: Context, type: MiniPost.PostsType, lastId: Int, groupId: Int) {
            val intent = Intent(context, SyncNewService::class.java)
            intent.action = when (type) {
                MiniPost.PostsType.ALL -> ACTION_FETCH_NEW_POSTS_ALL
                MiniPost.PostsType.TOPICS -> ACTION_FETCH_NEW_POSTS_TOPICS
                MiniPost.PostsType.COLLECTION -> ACTION_FETCH_NEW_POSTS_COLLECTION
            }
            intent.putExtra(ID, groupId)
            intent.putExtra(LAST_ID, lastId)
            context.startService(intent)
        }
    }
}