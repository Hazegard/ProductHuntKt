package fr.ec.producthuntkt.repository.database

import android.arch.persistence.room.TypeConverter
import java.util.*

/**
 * Created by maxime on 22/02/18.
 */
class DateConverter {
    @TypeConverter
    fun toDate(timeStamp: Long?): Date? {
        return when (timeStamp) {
            null -> null
            else -> Date(timeStamp)
        }
    }

    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }
}