package fr.ec.producthuntkt


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import fr.ec.producthuntkt.model.retrofit.MiniPost
import kotlinx.android.synthetic.main.toolbarlayout.*
import kotlinx.android.synthetic.main.topics_list_activity.*


class MainActivity : AppCompatActivity() {

    private lateinit var drawerToggle: ActionBarDrawerToggle
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.topics_list_activity)
        setSupportActionBar(toolbar)
        toolbar.title = "Topics"
        supportFragmentManager.beginTransaction()
                .add(R.id.topicsLists_container, TopicFragment.getNewInstance())
                .commit()

        drawerToggle = ActionBarDrawerToggle(this@MainActivity, drawerLayout, toolbar,
                R.string.open, R.string.close)
        drawerLayout.addDrawerListener(drawerToggle)
        drawerToggle.syncState()
        nav_view.setNavigationItemSelectedListener {
            var fragment: Fragment? = null
            var fragmentTag = ""
            if (it.isChecked) {
                drawerLayout.closeDrawers()
                return@setNavigationItemSelectedListener true
            }
            when (it.itemId) {
                R.id.drawer_topic -> {
                    fragmentTag = "Topics"
                    fragment = supportFragmentManager.findFragmentByTag(fragmentTag) ?: TopicFragment.getNewInstance()
                }
                R.id.drawer_collections -> {
                    fragmentTag = "Collections"
                    return@setNavigationItemSelectedListener true
                }
                R.id.drawer_posts -> {
                    fragmentTag = "Posts"
                    fragment = supportFragmentManager.findFragmentByTag(fragmentTag) ?: ListPostFragment.getNewInstance(0, MiniPost.PostsType.ALL)

                }
                else -> {
                    return@setNavigationItemSelectedListener false
                }
            }
            supportFragmentManager.beginTransaction()
                    .replace(R.id.topicsLists_container, fragment, fragmentTag)
                    .addToBackStack(fragmentTag)
                    .commit()
            drawerLayout.closeDrawers()
            return@setNavigationItemSelectedListener true
        }
    }

    companion object {
        private const val TAG: String = "MainActivity"
    }
}
